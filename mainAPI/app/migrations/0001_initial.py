# Generated by Django 3.2.3 on 2021-06-01 03:49

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Sekolah',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sekolah_domain', models.CharField(max_length=100)),
                ('sekolah_bucket', models.CharField(max_length=200)),
                ('sekolah_secretKey', models.CharField(default='e(cv1u8MwET<@k', editable=False, max_length=250)),
            ],
        ),
    ]
