from django.db import models
from unique_id import get_unique_id

class Sekolah(models.Model):
	sekolah_domain=models.CharField(max_length=100)
	sekolah_bucket=models.CharField(max_length=200)
	sekolah_secretKey=models.CharField(max_length=250,default=get_unique_id(),editable=False)

